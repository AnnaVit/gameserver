# README #

Questo progetto rappresenta il Server da poter utilizzare per lo scambio di messaggi fra Hololens e un dispositivo Android.

### Istruzioni per l'esecuzione ###

Per prima cosa aprire il progetto in Visual Studio, dopodichè spostarsi nel file _ServerTCP_, inserire al posto della stringa "Indrizzo server" a riga 11 l'indirizzo IP del calcolatore su cui verrà eseguito il programma. Per saperlo aprite un prompt dei comandi e digitate il comando _ipconfig_ e prendete l'indirizzo ipv4.

Se all'esecuzione del programma in ambiente Windows, appare una dialog che vi chiede l'accesso alla rete, cliccate su consenti accesso, altrimenti il firewall non consentirà lo scambio di messaggi.

Se il server è riuscito a inizializzarsi correttamente dovrrebbe apparire una Console, in cui pter visualizzare i messaggi del server e i clients che si sono connessi o disconnessi.

### Note da ricordare ###

L'applicazione è pensata per poter ospitare un gruppo ristretto di persone al massimo 7, ciscuna delle quali identificata da un colore specifico, i client vengono gestititi tramite la classe _ClientMenager_ e le classi _DataSender_ e _DataReciver_ contengono la definizione dei pacchetti che possono essere inviati e ricevuti, con i rispettivi metodi di gestione.
