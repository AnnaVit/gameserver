﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Linq;

namespace GameServer
{
    static class ClientManager
    {
        public static Dictionary<int, Client> client = new Dictionary<int, Client>();
        public static List<string> colorAvailable = new List<string> { "Blue", "Cyan", "Green", "Magenta", "Red", "Yellow", "Gray" };

        public static void CreateNewConnection(TcpClient tempClient)
        {
            if (colorAvailable.Count != 0) 
            {
                Client newClient = new Client();
                newClient.Socket = tempClient;
                newClient.ConnectionID = ((IPEndPoint)tempClient.Client.RemoteEndPoint).Port;
                newClient.Color = colorAvailable.ElementAt(0);
                
                newClient.Start();
                colorAvailable.RemoveAt(0);

                client.Add(newClient.ConnectionID, newClient);
                DataSender.sendWelcomeMessage(newClient.ConnectionID);
            }

            

        }

        public static void RestoreColor(string color)
        {
            colorAvailable.Add(color);
        }

        public static void SendDataTo(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt(data.GetUpperBound(0) - data.GetLowerBound(0) + 1); //no data.lenght perchè se ci sono più array in data potrebbe dare degli errori
            buffer.WriteBytes(data);
            client[connectionID].Stream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
            buffer.Dispose();
        }
    }
}
