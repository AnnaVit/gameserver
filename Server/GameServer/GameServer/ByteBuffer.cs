﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer
{
    public class ByteBuffer : IDisposable
    {
        private List<Byte> buff;
        private byte[] readBuff;
        private int readPos;
        private bool buffUpdated;
        private bool dispose;

        public ByteBuffer()
        {
            this.buff = new List<Byte>();
            this.readPos = 0;
            this.buffUpdated = false;
            this.dispose = false;
        }

        public int GetReadPosition() 
        {
            return this.readPos;
        }

        public byte[] ToArray()
        {
            return this.buff.ToArray();
        }

        public int Count()
        {
            return this.buff.Count;
        }

        public int Lenght()
        {
            return this.Count() - this.readPos;
        }

        public void Clear()
        {
            this.buff.Clear();
            this.readPos = 0;
        }

        public void WriteByte(byte input)
        {
            this.buff.Add(input);
            this.buffUpdated = true;
        }

        public void WriteBytes(byte[] input)
        {
            this.buff.AddRange(input);
            this.buffUpdated = true;
        }

        public void WriteShort(short input)
        {
            this.buff.AddRange(BitConverter.GetBytes(input));
            this.buffUpdated = true;
        }

        public void WriteInt(int input)
        {
            this.buff.AddRange(BitConverter.GetBytes(input));
            this.buffUpdated = true;
        }

        public void WriteLong(long input)
        {
            this.buff.AddRange(BitConverter.GetBytes(input));
            this.buffUpdated = true;
        }

        public void WriteFloat(float input)
        {
            this.buff.AddRange(BitConverter.GetBytes(input));
            this.buffUpdated = true;
        }

        public void WriteBool(bool input)
        {
            this.buff.AddRange(BitConverter.GetBytes(input));
            this.buffUpdated = true;
        }

        public void WriteString(string input)
        {
            this.buff.AddRange(BitConverter.GetBytes(input.Length));
            this.buff.AddRange(Encoding.ASCII.GetBytes(input));
            this.buffUpdated = true;
        }

        public byte ReadByte(bool peek = true) 
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                byte value = this.readBuff[this.readPos];

                if (peek & this.buff.Count > readPos)
                {
                    readPos += 1;
                }

                return value;
            }
            else 
            {
                throw new Exception("Not reading a byte");
            }
        }

        public byte[] ReadBytes(int lenght, bool peek = true)
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                byte[] value = this.buff.GetRange(this.readPos, lenght).ToArray();

                if (peek)
                {
                    readPos += lenght;
                }

                return value;
            }
            else
            {
                throw new Exception("Not reading a byte array");
            }
        }

        public short ReadShort(bool peek = true)
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                short value = BitConverter.ToInt16(this.readBuff, this.readPos);

                if (peek & this.buff.Count > this.readPos)
                {
                    readPos += 2;
                }

                return value;
            }
            else
            {
                throw new Exception("Not reading a short");
            }
        }

        public int ReadInt(bool peek = true)
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                int value = BitConverter.ToInt32(this.readBuff, this.readPos);

                if (peek & this.buff.Count > this.readPos)
                {
                    readPos += 4;
                }

                return value;
            }
            else
            {
                throw new Exception("Not reading a int");
            }
        }

        public long ReadLong(bool peek = true)
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                long value = BitConverter.ToInt64(this.readBuff, this.readPos);

                if (peek & this.buff.Count > this.readPos)
                {
                    readPos += 8;
                }

                return value;
            }
            else
            {
                throw new Exception("Not reading a long");
            }
        }

        public float ReadFloat(bool peek = true)
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                float value = BitConverter.ToSingle(this.readBuff, this.readPos);

                if (peek & this.buff.Count > this.readPos)
                {
                    readPos += 4;
                }

                return value;
            }
            else
            {
                throw new Exception("Not reading a float");
            }
        }

        public bool ReadBool(bool peek = true)
        {
            if (this.buff.Count > this.readPos)
            {
                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                bool value = BitConverter.ToBoolean(this.readBuff, this.readPos);

                if (peek & this.buff.Count > this.readPos)
                {
                    readPos += 1;
                }

                return value;
            }
            else
            {
                throw new Exception("Not reading a bool");
            }
        }

        public string ReadString(bool peek = true)
        {

            try
            {
                int lenght = ReadInt(true); //leggiamo lunghezza stringa prima

                if (this.buffUpdated)
                {
                    this.readBuff = this.buff.ToArray();
                    this.buffUpdated = false;
                }

                string value = Encoding.ASCII.GetString(this.readBuff, this.readPos, lenght);

                if (peek & this.buff.Count > this.readPos)
                {
                    if (value.Length > 0)
                    {
                        this.readPos += lenght;
                    }
                }

                return value;
            }
            catch (Exception)
            {
                throw new Exception("Not reading a String");
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                if (disposing)
                {
                    this.buff.Clear();
                    this.readPos = 0;
                }
                this.dispose = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
