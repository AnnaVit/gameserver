﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GameServer
{
    public enum ServerPackets
    { 
        SWelcomeMessage = 1,

        SRemoveControls = 2,

        SRestoreControl = 3,

        SUpdatePosition = 4,

        SUpdateRotation = 5,

        SUpdateScale = 6,

        SUpdateEulerAngle = 7,

    }

    static class DataSender
    {

        private static void SendMessage(int connectionID, int packetCode, string message)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt(packetCode);
            buffer.WriteString(message);
            ClientManager.SendDataTo(connectionID, buffer.ToArray());
            buffer.Dispose();
        }

        public static void sendWelcomeMessage(int connectionID)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ServerPackets.SWelcomeMessage);

            ClientManager.client.TryGetValue(connectionID, out Client client);
                buffer.WriteString(client.Color);
                ClientManager.SendDataTo(connectionID, buffer.ToArray());
                buffer.Dispose();
           
        }

        public static void sendRemoveControl(int connectionID, string msg)
        {
            SendMessage(connectionID, (int) ServerPackets.SRemoveControls, msg);
        }

        public static void sendRestoreControl(int connectionID)
        {
            SendMessage(connectionID, (int)ServerPackets.SRestoreControl, "Remove control");
        }

        public static void sendUpdatePosition(int connectionID, string msg)
        {
            SendMessage(connectionID, (int)ServerPackets.SUpdatePosition, msg);
        }

        public static void sendUpdateRotation(int connectionID, string msg)
        {
            SendMessage(connectionID, (int)ServerPackets.SUpdateRotation, msg);
        }

        public static void sendUpdateScale(int connectionID, string msg)
        {
            SendMessage(connectionID, (int)ServerPackets.SUpdateScale, msg);
        }

        public static void sendUpdateEulerAngle(int connectionID, string msg)
        {
            SendMessage(connectionID, (int)ServerPackets.SUpdateEulerAngle, msg);
        }
    }
}
