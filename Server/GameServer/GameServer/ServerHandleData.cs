﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer
{
    //ogni pacchetto ha il proprio identificatore e il server sa come gestirlo
    static class ServerHandleData
    {
        private static Dictionary<int, Packet> packets = new Dictionary<int, Packet>();


        public delegate void Packet(int connectionId, byte[] data);

        public static void InitializePackets()
        {
            packets.Add((int)ClientPackets.CHelloServer, DataReceiver.HandleHelloServer);
            packets.Add((int)ClientPackets.CPressButton, DataReceiver.HandlePressButton);
            packets.Add((int)ClientPackets.CRotation, DataReceiver.HandleRotation);
            packets.Add((int)ClientPackets.CScale, DataReceiver.HandleScale);
            packets.Add((int)ClientPackets.CPosition, DataReceiver.HandlePosition);
            packets.Add((int)ClientPackets.CEulerAngle, DataReceiver.HandleEulerAngle);
            packets.Add((int)ClientPackets.CRequestControl, DataReceiver.HandleRequestControl);
            packets.Add((int)ClientPackets.CReleseControl, DataReceiver.HandleReleseControl);
        }

        //prevents package lost
        public static void HandleData(int connectionID, byte[] data)
        {
            byte[] buffer = (byte[])data.Clone();
            int pLength = 0;

            if (ClientManager.client[connectionID].Buffer == null)
            {
                ClientManager.client[connectionID].Buffer = new ByteBuffer();
            }

            ClientManager.client[connectionID].Buffer.WriteBytes(buffer);

            if (ClientManager.client[connectionID].Buffer.Count() == 0)
            {
                ClientManager.client[connectionID].Buffer.Clear();
                return;
            }

            if (ClientManager.client[connectionID].Buffer.Lenght() >= 4)
            {
                pLength = ClientManager.client[connectionID].Buffer.ReadInt(false);

                if (pLength <= 0)
                {
                    ClientManager.client[connectionID].Buffer.Clear();
                    return;
                }

            }

            while (pLength > 0 & pLength <= ClientManager.client[connectionID].Buffer.Lenght() - 4) //prima leggiamo l'int della lunghezza
            {
                if (pLength <= ClientManager.client[connectionID].Buffer.Lenght() - 4)
                {
                    ClientManager.client[connectionID].Buffer.ReadInt();
                    data = ClientManager.client[connectionID].Buffer.ReadBytes(pLength);
                    HandleDataPackets(connectionID, data);
                }

                pLength = 0;

                if (ClientManager.client[connectionID].Buffer.Lenght() >= 4)
                {
                    pLength = ClientManager.client[connectionID].Buffer.ReadInt(false);

                    if (pLength <= 0)
                    {
                        ClientManager.client[connectionID].Buffer.Clear();
                        return;
                    }
                }
            }

            if (pLength <= 1)
            {
                ClientManager.client[connectionID].Buffer.Clear();
            }
        }

        private static void HandleDataPackets(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            buffer.Dispose();

            if (packets.TryGetValue(packetID, out Packet packet))
            {
                packet.Invoke(connectionID, data);
            }

        }
    }
}
