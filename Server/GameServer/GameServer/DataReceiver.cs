﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer
{
    public enum ClientPackets
    { 
        CHelloServer = 1,

        CPressButton = 2,

        CRotation = 3,

        CScale = 4,

        CPosition = 5,

        CRequestControl = 6,

        CReleseControl = 7,

        CEulerAngle = 8,
    }

    static class DataReceiver
    {

        private static string ReadMessage(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            return msg;
        }

        public static void HandleHelloServer(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);
            Console.WriteLine(msg);
        }

        public static void HandlePressButton(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);
           Console.WriteLine(connectionID.ToString() + " " + msg);
        }

        public static void HandleRotation(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);
            
            foreach (Client elem in ClientManager.client.Values)
            {
                if (elem.ConnectionID != connectionID)
                {
                    DataSender.sendUpdateRotation(elem.ConnectionID, msg);
                }
            }

            //Console.WriteLine(connectionID.ToString() + " " + msg);
        }

        public static void HandleScale(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);

            foreach (Client elem in ClientManager.client.Values)
            {
                if (elem.ConnectionID != connectionID)
                {
                    DataSender.sendUpdateScale(elem.ConnectionID, msg);
                }
            }

            //Console.WriteLine(connectionID.ToString() + " " + msg);
        }

        public static void HandlePosition(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);

            foreach (Client elem in ClientManager.client.Values)
            {
                if (elem.ConnectionID != connectionID)
                {
                    DataSender.sendUpdatePosition(elem.ConnectionID, msg);
                }
            }

           // Console.WriteLine(connectionID.ToString() + " " + msg);
        }

        public static void HandleEulerAngle(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);

            foreach (Client elem in ClientManager.client.Values)
            {
                if (elem.ConnectionID != connectionID)
                {
                    DataSender.sendUpdateEulerAngle(elem.ConnectionID, msg);
                }
            }

           // Console.WriteLine(connectionID.ToString() + " " + msg);
        }


        public static void HandleRequestControl(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);

            foreach (Client elem in ClientManager.client.Values)
            {
                if (elem.ConnectionID != connectionID)
                {
                    Console.WriteLine("send to " + elem.ConnectionID.ToString() + " remove control");
                    DataSender.sendRemoveControl(elem.ConnectionID, msg);
                }
            }
        }

        public static void HandleReleseControl(int connectionID, byte[] data)
        {
            string msg = ReadMessage(data);

            foreach (Client elem in ClientManager.client.Values)
            {
                if (elem.ConnectionID != connectionID)
                {
                    Console.WriteLine("send to " + elem.ConnectionID.ToString() + " remove control");
                    DataSender.sendRestoreControl(elem.ConnectionID);
                }
            }
        }
    }
}
