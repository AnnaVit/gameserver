﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace GameServer
{
    public class Client
    {
        private int connectionID;
        private TcpClient socket;
        private NetworkStream stream;
        private byte[] recBuffer;
        private ByteBuffer buffer;

        private string color;

        public int ConnectionID { get => connectionID; set => connectionID = value; }
        public TcpClient Socket { get => socket; set => socket = value; }
        public NetworkStream Stream { get => stream; set => stream = value; }
        public ByteBuffer Buffer { get => buffer; set => buffer = value; }
        public string Color { get => color; set => color = value; }

        public void Start()
        {
            //setup connection
            this.socket.SendBufferSize = 4096;
            this.socket.ReceiveBufferSize = 4096;
            this.stream = this.socket.GetStream();
            this.recBuffer = new byte[4096];
            this.stream.BeginRead(this.recBuffer, 0, this.socket.ReceiveBufferSize, OnReciveData, null);
            Console.WriteLine("Incoming connection from '{0}'", this.socket.Client.RemoteEndPoint.ToString());
        }


        //chiamata ogni volta che viene ricevuto un pacchetto
        private void OnReciveData(IAsyncResult result)
            {
            try
            {
                int lenght = this.stream.EndRead(result);
                if (lenght <= 0)
                {
                    CloseConnection();
                    return;
                }

                byte[] newBytes = new byte[lenght];
                Array.Copy(this.recBuffer, newBytes, lenght);

                ServerHandleData.HandleData(ConnectionID, newBytes);

                this.stream.BeginRead(this.recBuffer, 0, this.socket.ReceiveBufferSize, OnReciveData, null);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CloseConnection()
        {
            Console.WriteLine("Connection from '{0}' has been terminated", this.socket.Client.RemoteEndPoint.ToString());
            this.socket.Close();

            foreach (var elem in ClientManager.client) {
                if (elem.Value.Equals(this)) {
                    ClientManager.RestoreColor(color);
                    ClientManager.client.Remove(elem.Key);
                }
            }
        }
    }
}
