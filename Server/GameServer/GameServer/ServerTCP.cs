﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace GameServer
{
    static class ServerTCP
    {
        static TcpListener serverSockets = new TcpListener(IPAddress.Parse("192.168.110.233"), 8080);

        public static void InitializeNetwork()
        {
            Console.WriteLine("Initialize connection...");
            ServerHandleData.InitializePackets();
            serverSockets.Start();
            serverSockets.BeginAcceptTcpClient(new AsyncCallback(OnClientConnect), null);
        }

        private static void OnClientConnect(IAsyncResult result)
        {
            TcpClient client = serverSockets.EndAcceptTcpClient(result);
            serverSockets.BeginAcceptTcpClient(new AsyncCallback(OnClientConnect), null); //ci mettiamo in attesa di altre connessioni
            ClientManager.CreateNewConnection(client);
        }
    }
}